$(document).ready(function(){

    $('#GovernanceModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget);
      var recipient = button.data('slide');
      $('#GovernanceModal').carousel(recipient);
    });

    $('#ProductModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget);
      var recipient = button.data('slide');
      $('#ProductModal').carousel(recipient);
    });

    $('#ShariahModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget);
      var recipient = button.data('slide');
      $('#ShariahModal').carousel(recipient);
    });

    $('#BusinessModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget);
      var recipient = button.data('slide');
      $('#BusinessModal').carousel(recipient);
    });

    $('#MarketingModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget);
      var recipient = button.data('slide');
      $('#MarketingModal').carousel(recipient);
    });

    $('.cst-carousel-control').click(function(e){
      e.preventDefault();
      var target = $(this).closest('.modal').attr('id');
      console.log(target);
      if($(this).hasClass('next'))
        $('#'+target).carousel('next');
      if($(this).hasClass('prev'))
        $('#'+target).carousel('prev');
    });

    $('.handle').click(function(e){
      e.preventDefault();
      $(this).toggleClass('down up');
      $(this).parents('.team-accordion').find('.teamrow').toggleClass('hide show');

    });

    if($(window).width() <= 992)
    {
      $('#mainNav').addClass('solid');
    }

    $(window).scroll(function(){
      var sticky = $('#mainNav'),
          scroll = $(window).scrollTop();
      if($(window).width() > 992)
      {
        if (scroll >= 100) sticky.addClass('solid');
        else sticky.removeClass('solid');
      }
    });

    // $('.movingletters').each(function(){
    //   // Wrap every letter in a span
    //   $(this).innerHTML = $(this).textContent.replace(/\S/g, "<span class='letter'>$&</span>");

    //   anime.timeline()
    //     .add({
    //       targets: $(this).find('.letter'),
    //       translateY: ["1.1em", 0],
    //       translateZ: 0,
    //       duration: 750,
    //       delay: (el, i) => 50 * i
    //     });
    // })

  });

  // Wrap every letter in a span for moving letters
  document.querySelectorAll('.movingletters').forEach(item => {
    item.innerHTML = item.textContent.replace(/\S/g, "<span class='letter'>$&</span>");
  });

  // requestAnimationFrame

  const raf = 
    window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    function( callback ) {
      window.setTimeout( callback, 1000 / 60 )
    };

  // The checker for el to be in viewport
  const gambitGalleryIsInView = el => {
    const scroll = window.scrollY || window.pageYOffset
    const boundsTop = el.getBoundingClientRect().top + scroll
    const viewport = {
      top: scroll,
      bottom: scroll + window.innerHeight,
    }
    const bounds = {
      top: boundsTop,
      bottom: boundsTop + el.clientHeight,
    }
    return ( bounds.bottom >= viewport.top && bounds.bottom <= viewport.bottom ) || ( bounds.top <= viewport.bottom && bounds.top >= viewport.top );
  }

  // Usage to check if moving letter element is in viewport
  document.addEventListener( 'DOMContentLoaded', () => {
    const scrollingel = document.getElementsByClassName( 'movingletters' );
    const handler = () => raf( () => {
      for(var z = 0; z < scrollingel.length; z++) 
      {
        var topull = scrollingel[z];
        if(gambitGalleryIsInView(topull) && !topull.classList.contains("pulled"))
        {
          var elm = topull.getElementsByClassName('letter');
          anime.timeline()
          .add({
            targets: elm,
            translateY: ["1.1em", 0],
            translateZ: 0,
            duration: 750,
            delay: (el, i) => 50 * i
          });

          topull.classList.add('pulled');
        }
      }

      //check cardbox and add class to flip
      document.querySelectorAll('.cardbox').forEach(item => {
        //item.innerHTML = item.textContent.replace(/\S/g, "<span class='letter'>$&</span>");
        if(gambitGalleryIsInView(item))
        {
          item.classList.add('animate__flipInX');
        }
      });

      //check fadeup element in view and add class
      document.querySelectorAll('.fadeup').forEach(item => {
        //item.innerHTML = item.textContent.replace(/\S/g, "<span class='letter'>$&</span>");
        if(gambitGalleryIsInView(item) && !item.classList.contains('animate__fadeInUp'))
        {
          item.classList.add('animate__fadeInUp');
        }
      });

      //check slidedown element in view and add class
      document.querySelectorAll('.slidedown').forEach(item => {
        //item.innerHTML = item.textContent.replace(/\S/g, "<span class='letter'>$&</span>");
        if(gambitGalleryIsInView(item) && !item.classList.contains('animate__slideInDown'))
        {
          item.classList.add('animate__slideInDown');
        }
      });

      //check slidedown element in view and add class
      document.querySelectorAll('.slideleft').forEach(item => {
        //item.innerHTML = item.textContent.replace(/\S/g, "<span class='letter'>$&</span>");
        if(gambitGalleryIsInView(item) && !item.classList.contains('animate__slideInLeft'))
        {
          item.classList.add('animate__slideInLeft');
        }
      });

      //check slidedown element in view and add class
      document.querySelectorAll('.shape-1').forEach(item => {
        //item.innerHTML = item.textContent.replace(/\S/g, "<span class='letter'>$&</span>");
        if(gambitGalleryIsInView(item))
        {
          item.classList.add('inview');
        }
      });

      //check slidedown element in view and add class
      document.querySelectorAll('.shape-2').forEach(item => {
        //item.innerHTML = item.textContent.replace(/\S/g, "<span class='letter'>$&</span>");
        if(gambitGalleryIsInView(item))
        {
          item.classList.add('inview');
        }
      });

    })
    handler()
    window.addEventListener( 'scroll', handler )
  });